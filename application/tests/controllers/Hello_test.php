<?php
class Hello_test extends CI_Controller
{
    public function test_get_hello()
    {
        // $this->loadview("hello_view");
        $output = $this->request('GET',['Hello','get_hello']);
        $expected = '<h2>hello</h2>';

        $this->assertContains($expected,$output);
    }
}